package in.co.rshop.ems.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Hostel {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long Hostelid;
	private String HostelName;
	private int NoOfSeats;

	

	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	public long getHostelid() {
		return Hostelid;
	}

	public void setHostelid(long hostelid) {
		Hostelid = hostelid;
	}

	public String getHostelName() {
		return HostelName;
	}

	public void setHostelName(String hostelName) {
		HostelName = hostelName;
	}

	public int getNoOfSeats() {
		return NoOfSeats;
	}

	public void setNoOfSeats(int noOfSeats) {
		NoOfSeats = noOfSeats;
	}

}
