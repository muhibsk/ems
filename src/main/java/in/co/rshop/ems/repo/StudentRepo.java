package in.co.rshop.ems.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import in.co.rshop.ems.entities.Student;

public interface StudentRepo extends JpaRepository<Student, Long>{

}
